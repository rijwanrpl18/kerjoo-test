<?php

namespace Tests\Feature;

use App\AnnualLeave;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AnnualLeaveTest extends TestCase
{
	use WithFaker;

	/**
	 * get list annual leave
	 *
	 * @return void
	 */
	public function testListAnnualLeave()
	{
		$response = $this->get('/api/annual-leaves');
		$response->assertStatus(200);
	}

	/**
	 * create new annual leave - success
	 *
	 * @return void
	 */
	public function testCreateAnnualLeave()
	{
		$response = $this->json('POST', '/api/annual-leaves', [
			'start_date' => '2022-01-01',
			'end_date' => '2022-01-02',
			'reason' => $this->faker->randomLetter(),
			'user_id' => User::all()->random()->id,
		]);

		$response->assertStatus(200);
	}

	/**
	 * create new annual leave - error request
	 *
	 * @return void
	 */
	public function testCreateAnnualLeaveErrorRequest()
	{
		$response = $this->json('POST', '/api/annual-leaves', [
			'start_date' => '2022-01-01',
			'end_date' => '2022-01-02',
		]);

		$response->assertStatus(422);
	}

	/**
	 * create new annual leave - error date
	 *
	 * @return void
	 */
	public function testCreateAnnualLeaveErrorDate()
	{
		$response = $this->json('POST', '/api/annual-leaves', [
			'start_date' => '2022-01-10',
			'end_date' => '2022-01-02',
			'reason' => $this->faker->randomLetter(),
			'user_id' => User::all()->random()->id,
		]);

		$response->assertStatus(400);
	}

	/**
	 * get one annual leave
	 *
	 * @return void
	 */
	public function testGetOneAnnualLeave()
	{
		$id = AnnualLeave::all()->random()->id;
		$response = $this->get("/api/annual-leaves/$id");
		$response->assertStatus(200);
	}

	/**
	 * get one annual leave - error not fount
	 *
	 * @return void
	 */
	public function testGetOneAnnualLeaveNotFound()
	{
		$id = 0;
		$response = $this->get("/api/annual-leaves/$id");
		$response->assertStatus(400);
	}
}
