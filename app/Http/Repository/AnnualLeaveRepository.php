<?php

namespace App\Http\Repository;

use App\AnnualLeave;

class AnnualLeaveRepository
{
	/**
	 * Get list annual leave
	 * @param array $param
	 *
	 * @return object $data
	 */
	public function list(array $param)
	{
		$query = AnnualLeave::query();
		if (isset($param['user_id'])) {
			$query = $query->where('user_id', $param['user_id']);
		}
		$data = $query->paginate($param['per_page']);
		return $data;
	}

	/**
	 * create new annual leave
	 * @param array $param
	 *
	 * @return object $data
	 */
	public function store(array $param)
	{
		$data = AnnualLeave::create($param);
		return $data;
	}

	/**
	 * get one annual leave
	 * @param int $id
	 *
	 * @return object $data
	 */
	public function get_one(int $id)
	{
		$data = AnnualLeave::with('user')->find($id);
		return $data;
	}
}