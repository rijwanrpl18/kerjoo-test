<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
	/**
	 * Test grid
	 * FE Test soal a
	 */
	public function test_grid(Request $request)
	{
		return view('test-grid');
	}

	/**
	 * Test flex
	 * FE Test soal b
	 */
	public function test_flex(Request $request)
	{
		return view('test-flex');
	}
}
