<?php

namespace App\Http\Controllers;

use App\Http\Repository\AnnualLeaveRepository;
use App\Http\Requests\AnnualLeaveStore;
use Illuminate\Http\Request;

class AnnualLeaveController extends Controller
{
	protected $annual_leave;
	public function __construct(AnnualLeaveRepository $annual_leave) {
		$this->annual_leave = $annual_leave;
	}

	/**
	 * Format Response API
	 * @param array $response
	 *
	 * @return json $response
	 */
	private function api_response(array $response)
	{
		$error_code = isset($response['error_code']) ? $response['error_code'] : 0;

		return response()->json([
			'data' => $response['data'],
			'message' => $response['message'],
			'error_code' => $error_code,
		], $response['http_code']);
	}

	/**
	 * List annual leave
	 *
	 * @return json $response
	 */
	public function index(Request $request)
	{
		$response = [
			'http_code' => 200,
			'data' => [],
			'message' => '',
		];
		try {
			// set per_page data, default 10
			$param['per_page'] = $request->get('per_page', 10);
			// check existing parameter user_id
			if ($request->has('user_id')) {
				$param['user_id'] = $request->user_id;
			}
			// get list annual leave
			$annual_leaves = $this->annual_leave->list($param);
			$response['data'] = $annual_leaves;
		} catch (\Throwable $th) {
			$response['http_code'] = 400;
			$response['message'] = $th->getMessage();
		}
		return $this->api_response($response);
	}

	/**
	 * Create new annual leave
	 *
	 * @return json $response
	 */
	public function store(AnnualLeaveStore $request)
	{
		$response = [
			'http_code' => 200,
			'data' => [],
			'message' => '',
		];
		try {
			$param = $request->all();
			if ($param['start_date']>$param['end_date']) {
				throw new \Exception("Start date cannot be higher than end date");
			}
			$annual_leave = $this->annual_leave->store($param);
			$response['data'] = $annual_leave;
		} catch (\Throwable $th) {
			$response['http_code'] = 400;
			$response['message'] = $th->getMessage();
		}
		return $this->api_response($response);
	}

	/**
	 * Show annual leave
	 *
	 * @return json $response
	 */
	public function show($id)
	{
		$response = [
			'http_code' => 200,
			'data' => [],
			'message' => '',
		];
		try {
			$annual_leave = $this->annual_leave->get_one($id);
			if (!$annual_leave) {
				throw new \Exception('Annual leave not found');
			}
			$response['data'] = $annual_leave;
		} catch (\Throwable $th) {
			$response['http_code'] = 400;
			$response['message'] = $th->getMessage();
		}
		return $this->api_response($response);
	}
}
