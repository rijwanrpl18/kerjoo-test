<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\AnnualLeave;
use App\User;
use Faker\Generator as Faker;

$factory->define(AnnualLeave::class, function (Faker $faker) {
    $end_date = $faker->date('Y-m-d');
    $start_date = $faker->date('Y-m-d', $end_date);
    return [
        'start_date' => $start_date,
        'end_date' => $end_date,
        'user_id' => User::all()->random()->id,
        'reason' => $faker->randomLetter(),
    ];
});
