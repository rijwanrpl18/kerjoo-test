@extends('master')

@section('contents')
	<div class="d-flex flex-md-row flex-column mb-3">
		<div class="p-2">
			<button class="btn btn-success">Tambah</button>
			<button class="btn btn-success">Import</button>
			<button class="btn btn-success">Export</button>
		</div>
		<div class="d-flex flex-sm-row flex-column mb-3">
			<div class="p-2">
				<input type="text" name="keyword" id="keyword" class="form-control" placeholder="search">
			</div>
			<div class="p-2">
				<select class="form-select" aria-label="Tahun">
					<option disabled>Tahun</option>
					<option value="2020">2020</option>
					<option value="2021">2021</option>
					<option value="2022">2022</option>
				</select>
			</div>
		</div>
	</div>
@endsection